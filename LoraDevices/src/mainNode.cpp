
#include <Arduino.h>
#include "config.h"

#ifdef NODE
#include "Transaction.h"
#include "HttpEth.h"
#include "Wallet.h"
#include "Utils.h"
#include "OLED.h"
#include "LoraNode.h"

HttpEth httpEth;
LoraNode lora;
OLED display;

String buildTransaction(String to_add, String private_key, int nonce, int price, int limit, int amount, String functionName) {
    Transaction tx(nonce, price, limit, to_add, amount, functionName);
    tx.sign(private_key);
    return tx.getSigned();
}

String buildGetterTransaction(char* from_add, char* to_add, String data, String private_key) {
    Transaction tx(from_add, to_add, data);
    tx.sign(private_key);
    return tx.getSigned();
}

String functionCallToData(String functionName, String par1, unsigned long long par2) {
    String aux1 = Utils::pad32Bytes(par1);
    String aux2 = Utils::pad32Bytes(Utils::longDecimalToHexString(par2));
    uint8_t buf[50];
    functionName.getBytes(buf, 50);
    return Utils::hash(buf, functionName.length()).substring(0, 8) + aux1 + aux2;
}

String functionGetValue(String functionName) {
    uint8_t buf[50];
    functionName.getBytes(buf, 50);
    return Utils::hash(buf, functionName.length()).substring(0, 8);
}

String getJSONRPC(String method, String params) {
    return "{\"jsonrpc\": \"2.0\", \"method\": \"" + method + "\", \"params\": [" + params + "], \"id\":1}";
}

String getParamsWithData(String data) {
    return "{\"from\": \"" + String(MY_ADDRESS) + "\", \"to\": \"" + String(SC_ADDRESS) + "\", \"chainId\": \"" + String(CHAIN_ID) + "\", \"data\": \"0x" + data + "\"}";
}

String getCallJSON(String functionName) {
    String parsedFunctionName = functionGetValue(functionName);
    String params = getParamsWithData(parsedFunctionName);
    return getJSONRPC("eth_call", params);
}

String getRawTransactionJSONRPC(String params) {
    return getJSONRPC("eth_sendRawTransaction", params);
}

String getTransactionCount(String address) {
    return getJSONRPC("eth_getTransactionCount", "\"" + address + "\", \"latest\"");
}

String getFunctionCallToDataTest(String functionName) {
    String address = String(MY_ADDRESS);

    //Remove the 0x from the address
    address.remove(0, 2);

    String parsedAddress = Utils::pad32Bytes(address);
    String parsedFunctionName = functionGetValue(functionName) + parsedAddress;
    String params = getParamsWithData(parsedFunctionName);
    return getJSONRPC("eth_call", params);
}

String getResultFromJSON(String json) {
    int index = json.indexOf("result");
    int finalIndex = json.indexOf("\"}");
    String result = json.substring(index + 9, finalIndex);
    return result;
}

int getNonce(String address) {
    String json = httpEth.sendRawTransaction(getTransactionCount(address));
    String result = getResultFromJSON(json);
    result.remove(0, 2);
    return Utils::hexStringToInt(result);
}

void setup() {
    Serial.begin(115200);
    Serial.println("Execution started");

    wallet.begin();
    Serial.print("pkey: ");
    Serial.println(wallet.getAddress());

    display.begin();
    display.scrollText(wallet.getAddress());

    lora.begin(wallet.getAddress().substring(0, 4));
    Serial.print("starting wifi connection: ");

    httpEth.listNetworks();
    httpEth.initConnection(RUT_SSID, WIFIPWD, SERVER_IP, SERVER_PORT);

    //Get owner function
    httpEth.sendRawTransaction(getCallJSON("getOwner()"));

    delay(1000);

    //Test function for 
    httpEth.sendRawTransaction(getFunctionCallToDataTest("balanceOf(address)"));
    String data = functionCallToData("transfer(address,uint256)", "c1D895D028eB2675e5232f5a2A33A80DC7d85E73", (unsigned long long) 10000000000000000000);
    int nonce = getNonce(String(MY_ADDRESS));

    int price = 5;
    int limit = 672197;
    int amount = 0;
    String sCAddress = String(SC_ADDRESS);
    sCAddress = sCAddress.substring(2, sCAddress.length());

    String trans = buildTransaction(sCAddress, MY_PRIVATEKEY, nonce, price, limit, amount, data);

    httpEth.sendRawTransaction(getRawTransactionJSONRPC("\"" + trans + "\""));
}

boolean runEvery(unsigned long interval) {
    static unsigned long previousMillis = 0;
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {
        previousMillis = currentMillis;
        return true;
    }
    return false;
}

void loop() {

    if (runEvery(SENDINGTIME)) { // repeat every 1000 millis
        // int nonce = 0;
        // int price = 50;
        // int limit = 21000;
        // int amount = 1;
        // Serial.print("\nin runEvery: ");
        // Serial.print("pkey: ");
        // String data = functionCallToData("transfer(address,uint256)", "337c67618968370907da31dAEf3020238D01c9de", (unsigned long long) 10000000000000000000);
        // Serial.print("pkey y: ");
        // String trans = buildTransaction(SC_ADDRESS, wallet.getPrivateKey(), nonce, price, limit, amount, data);
        //lora.sendMessage(trans);    
    }

}

#endif