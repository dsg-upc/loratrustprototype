# React Truffle Box

This box comes with everything you need to start using Truffle to write, compile, test, and deploy smart contracts, and interact with them from a React app.

## Installation

First ensure you are in an empty directory.

Run the `unbox` command using 1 of 2 ways.

```sh
# Install Truffle globally and run `truffle unbox`
$ npm install -g truffle
$ truffle unbox react
```

```sh
# Alternatively, run `truffle unbox` via npx
$ npx truffle unbox react
```

Start the react dev server.

```sh
$ cd client
$ npm start
  Starting the development server...
```

From there, follow the instructions on the hosted React app. It will walk you through using Truffle and Ganache to deploy the `SimpleStorage` contract, making calls to it, and sending transactions to change the contract's state.

## Commands

- Start Ganache

### Contracts
- Compile:         `cd truffle && truffle compile`

- Test:            `cd truffle && truffle test`

- Migrate:         `cd truffle && truffle migrate`

### DAPP

- Run dev server:       `cd client && npm start`

- Test:                 `cd client && npm test`

-  Build for production: `cd client && npm run build`

## FAQ

- __How do I use this with Ganache (or any other network)?__

  The Truffle project is set to deploy to Ganache by default. If you'd like to change this, it's as easy as modifying the Truffle config file! Check out [our documentation on adding network configurations](https://trufflesuite.com/docs/truffle/reference/configuration/#networks). From there, you can run `truffle migrate` pointed to another network, restart the React dev server, and see the change take place.

- __Where can I find more resources?__

  This Box is a sweet combo of [Truffle](https://trufflesuite.com) and [Create React App](https://create-react-app.dev). Either one would be a great place to start!


## Truffle commands

### Reset Deploy Contract
`truffle migrate --reset`

##3 Get ABI file

1. `truffle console`
2. `let instance = await LoraCoin.deployed()`
3. `instance.abi`

- `let accounts = await web3.eth.getAccounts()`
- `let balance = await instance.balanceOf(accounts[0])`
- `balance.toNumber()`

### ERC20 Methods
https://ethereum.org/en/developers/docs/standards/tokens/erc-20/

- `function name() public view returns (string)`
- `function symbol() public view returns (string)`
- `function decimals() public view returns (uint8)`
- `function totalSupply() public view returns (uint256)`
- `function balanceOf(address _owner) public view returns (uint256 balance)`
- `function transfer(address _to, uint256 _value) public returns (bool success)`
- `function transferFrom(address _from, address _to, uint256 _value) public returns (bool success)`
- `function approve(address _spender, uint256 _value) public returns (bool success)`
- `function allowance(address _owner, address _spender) public view returns (uint256 remaining)`
