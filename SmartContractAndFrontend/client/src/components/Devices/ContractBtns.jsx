import { useState } from "react";
import useEth from "../../contexts/EthContext/useEth";
import { ethers } from "ethers";

function ContractBtns({ setValue }) {
  const { state: { contract, accounts } } = useEth();
  const [inputValue, setInputValue] = useState("");

  const handleInputChange = e => {
    //   if (/^\d+$|^$/.test(e.target.value)) {
    setInputValue(e.target.value);
    //   }
  };

  const getOwner = async () => {
    const value = await contract.methods.getOwner().call({ from: accounts[0] });
    setValue(value);
  };

  const signMessage = async e => {
    if (e.target.tagName === "INPUT") {
      return;
    }
    if (inputValue === "") {
      alert("Please enter a value to write.");
      return;
    }

    const provider = new ethers.providers.Web3Provider(window.ethereum);

    const signer = provider.getSigner();
    console.log("Signer: " + signer);

    const signature = await signer.signMessage(inputValue);
    console.log("Signature: " + signature);

    const splitSig = ethers.utils.splitSignature(signature);

    console.log("v: " + splitSig.v + "\nr: " + splitSig.r + "\ns: " + splitSig.s);

    await contract.methods.createDevice(inputValue, splitSig.v, splitSig.r, splitSig.s).send({ from: accounts[0] }, function (err, res) {
      if (err) {
        console.log(err.message);
        // alert(err.message);
      }
    });

  }

  // const write = async e => {
  //   if (e.target.tagName === "INPUT") {
  //     return;
  //   }
  //   if (inputValue === "") {
  //     alert("Please enter a value to write.");
  //     return;
  //   }
  //   const newValue = parseInt(inputValue);
  //   await contract.methods.write(newValue).send({ from: accounts[0] });
  // };

  return (
    <div className="btns">

      <button onClick={getOwner}>
        getOwner()
      </button>

      <div onClick={signMessage} className="input-btn">
        signMessage(<input
          type="text"
          placeholder="uint"
          value={inputValue}
          onChange={handleInputChange}
        />)
      </div>

    </div>
  );
}

export default ContractBtns;
